package docker

// ContainerConfig config about containers
type ContainerConfig struct {
	ContainerName    string `json:"container_name"`
	ImageName        string `json:"image_name"`
	RepositoryURL    string `json:"repository_url"`
	Tty              bool   `json:"tty"`
	RestartOnNewCall bool   `json:"restart_on_new_call"`
	Autoremove       bool   `json:"auto_remove"`
}
