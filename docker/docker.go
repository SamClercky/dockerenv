package docker

import (
	"context"
	"fmt"
	"io"
	"os"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/client"
)

// Docker instance of Docker connection
type Docker struct {
	client *client.Client
	ctx    context.Context
}

// New constructor for Docker instance
func New() Docker {
	cli, err := client.NewEnvClient()
	if err != nil {
		panic(err)
	}

	return Docker{
		client: cli,
		ctx:    context.Background(),
	}
}

// Pull pulls an image down
func (d *Docker) Pull(imageName string) error {
	reader, err := d.client.ImagePull(
		d.ctx,
		imageName,
		types.ImagePullOptions{},
	)
	if err != nil {
		return err
	}

	io.Copy(os.Stdout, reader)

	return nil
}

// RunBackground creates a container and runs it in the background
func (d *Docker) RunBackground(config ContainerConfig) error {
	// First try to pull image before running
	err := d.Pull(config.RepositoryURL + config.ImageName)
	if err != nil {
		return err
	}

	// Create container
	resp, err := d.client.ContainerCreate(
		d.ctx,
		&container.Config{
			Image:      config.ImageName,
			Tty:        config.Tty,
			Entrypoint: []string{"tail", "-f", "/dev/null"}, // keep running forever
		},
		nil, nil, config.ContainerName,
	)
	if err != nil {
		return err
	}

	// Start container
	err = d.client.ContainerStart(
		d.ctx,
		resp.ID,
		types.ContainerStartOptions{},
	)
	if err != nil {
		return err
	}

	return nil
}

// ListContainers lists all open containers
func (d *Docker) ListContainers() ([]types.Container, error) {
	list, err := d.client.ContainerList(
		d.ctx,
		types.ContainerListOptions{},
	)
	if err != nil {
		return nil, err
	}

	return list, nil
}

// StopAllContainers stops all containers currently open
func (d *Docker) StopAllContainers() error {
	// List all containers
	list, err := d.ListContainers()
	if err != nil {
		return err
	}

	for _, cont := range list {
		fmt.Print("Stopping container ", cont.ID[:10], "... ")
		err := d.client.ContainerStop(
			d.ctx,
			cont.ID,
			nil,
		)
		if err != nil {
			fmt.Println("Error")
			continue
		}
		fmt.Println("Success")
	}
	return nil
}
