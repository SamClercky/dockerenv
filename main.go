package main

import (
	"gitlab.com/SamClercky/dockerenv/docker"
)

func main() {
	d := docker.New()

	err := d.RunBackground(docker.ContainerConfig{
		ContainerName: "Mijntest",
		ImageName:     "library/python",
		RepositoryURL: "docker.io/",
		Tty:           false,
	})
	if err != nil {
		panic(err)
	}
}
